/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.patchara.projectanimals;

/**
 *
 * @author user1
 */
public class TestAnimal {

    public static void main(String[] args) {
        Human h1 = new Human("Dang");
        h1.eat();
        h1.walk();
        h1.run();
        h1.speak();
        h1.sleep();
        System.out.println();

        Cat c1 = new Cat("Dam");
        c1.eat();
        c1.walk();
        c1.run();
        c1.speak();
        c1.sleep();
        System.out.println();

        Dog d1 = new Dog("Khew");
        d1.eat();
        d1.walk();
        d1.run();
        d1.speak();
        d1.sleep();
        System.out.println();

        Crocodile cc1 = new Crocodile("Leung");
        cc1.eat();
        cc1.walk();
        cc1.crawl();
        cc1.speak();
        cc1.sleep();
        System.out.println();

        Snake s1 = new Snake("Muang");
        s1.eat();
        s1.crawl();
        s1.speak();
        s1.sleep();
        System.out.println();

        Fish f1 = new Fish("Fah");
        f1.eat();
        f1.swim();
        f1.speak();
        f1.sleep();
        System.out.println();

        Crab cr1 = new Crab("NumGhen");
        cr1.eat();
        cr1.walk();
        cr1.swim();
        cr1.speak();
        cr1.sleep();
        System.out.println();

        Bird b1 = new Bird("Khaw");
        b1.eat();
        b1.walk();
        b1.fly();
        b1.speak();
        b1.sleep();
        System.out.println();

        Bat bt1 = new Bat("Som");
        bt1.eat();
        bt1.walk();
        bt1.fly();
        bt1.speak();
        bt1.sleep();
        System.out.println();

        //human
        System.out.println("h1 is animal?" + (h1 instanceof Animal));
        System.out.println("h1 is land animal?" + (h1 instanceof LandAnimal));
        System.out.println();

        //cat
        System.out.println("c1 is animal?" + (c1 instanceof Animal));
        System.out.println("c1 is land animal?" + (c1 instanceof LandAnimal));
        System.out.println();

        //dog
        System.out.println("d1 is animal?" + (d1 instanceof Animal));
        System.out.println("d1 is land animal?" + (d1 instanceof LandAnimal));
        System.out.println();

        //crocodile
        System.out.println("cc1 is animal?" + (cc1 instanceof Animal));
        System.out.println("cc1 is Reptile animal?" + (cc1 instanceof Reptile));
        System.out.println();

        //snake
        System.out.println("s1 is animal?" + (s1 instanceof Animal));
        System.out.println("s1 is Reptile animal?" + (s1 instanceof Reptile));
        System.out.println();

        //fist
        System.out.println("f1 is animal?" + (f1 instanceof Animal));
        System.out.println("f1 is Reptile animal?" + (f1 instanceof AquaticAnimal));
        System.out.println();

        //crab
        System.out.println("cr1 is animal?" + (cr1 instanceof Animal));
        System.out.println("cr1 is Reptile animal?" + (cr1 instanceof AquaticAnimal));
        System.out.println();

        //bird
        System.out.println("b1 is animal?" + (bt1 instanceof Animal));
        System.out.println("b1 is Reptile animal?" + (bt1 instanceof Poultry));
        System.out.println();

        //bat
        System.out.println("bt1 is animal?" + (bt1 instanceof Animal));
        System.out.println("bt1 is Reptile animal?" + (bt1 instanceof Poultry));
        System.out.println();

        Animal a1 = h1;
        Animal a2 = c1;
        Animal a3 = d1;
        Animal a4 = cc1;
        Animal a5 = s1;
        Animal a6 = f1;
        Animal a7 = cr1;
        Animal a8 = b1;
        Animal a9 = bt1;

        System.out.println("a1 is land animal?" + (a1 instanceof LandAnimal));
        System.out.println("a1 is reptile animal?" + (a1 instanceof Reptile));
        System.out.println("a1 is Poultry animal?" + (a1 instanceof Poultry));
        System.out.println("a1 is Aquatic animal?" + (a1 instanceof AquaticAnimal));
        System.out.println();
        
        System.out.println("a2 is land animal?" + (a2 instanceof LandAnimal));
        System.out.println("a2 is reptile animal?" + (a2 instanceof Reptile));
        System.out.println("a2 is Poultry animal?" + (a2 instanceof Poultry));
        System.out.println("a2 is Aquatic animal?" + (a2 instanceof AquaticAnimal));
        System.out.println();
        
        System.out.println("a3 is land animal?" + (a3 instanceof LandAnimal));
        System.out.println("a3 is reptile animal?" + (a3 instanceof Reptile));
        System.out.println("a3 is Poultry animal?" + (a3 instanceof Poultry));
        System.out.println("a3 is Aquatic animal?" + (a3 instanceof AquaticAnimal));
        System.out.println();
        
        System.out.println("a4 is land animal?" + (a4 instanceof LandAnimal));
        System.out.println("a4 is reptile animal?" + (a4 instanceof Reptile));
        System.out.println("a4 is Poultry animal?" + (a4 instanceof Poultry));
        System.out.println("a4 is Aquatic animal?" + (a4 instanceof AquaticAnimal));
        System.out.println();
        
        System.out.println("a5 is land animal?" + (a5 instanceof LandAnimal));
        System.out.println("a5 is reptile animal?" + (a5 instanceof Reptile));
        System.out.println("a5 is Poultry animal?" + (a5 instanceof Poultry));
        System.out.println("a5 is Aquatic animal?" + (a5 instanceof AquaticAnimal));
        System.out.println();
        
        System.out.println("a6 is land animal?" + (a6 instanceof LandAnimal));
        System.out.println("a6 is reptile animal?" + (a6 instanceof Reptile));
        System.out.println("a6 is Poultry animal?" + (a6 instanceof Poultry));
        System.out.println("a6 is Aquatic animal?" + (a6 instanceof AquaticAnimal));
        System.out.println();
        
        System.out.println("a7 is land animal?" + (a7 instanceof LandAnimal));
        System.out.println("a7 is reptile animal?" + (a7 instanceof Reptile));
        System.out.println("a7 is Poultry animal?" + (a7 instanceof Poultry));
        System.out.println("a7 is Aquatic animal?" + (a7 instanceof AquaticAnimal));
        System.out.println();
        
        System.out.println("a8 is land animal?" + (a8 instanceof LandAnimal));
        System.out.println("a8 is reptile animal?" + (a8 instanceof Reptile));
        System.out.println("a8 is Poultry animal?" + (a8 instanceof Poultry));
        System.out.println("a8 is Aquatic animal?" + (a8 instanceof AquaticAnimal));
        System.out.println();
        
        System.out.println("a9 is land animal?" + (a9 instanceof LandAnimal));
        System.out.println("a9 is reptile animal?" + (a9 instanceof Reptile));
        System.out.println("a9 is Poultry animal?" + (a9 instanceof Poultry));
        System.out.println("a9 is Aquatic animal?" + (a9 instanceof AquaticAnimal));
        System.out.println();

    }
}
